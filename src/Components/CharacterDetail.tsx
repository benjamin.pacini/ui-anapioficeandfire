import { Tag, Descriptions, List, Spin, Space, Typography } from 'antd';
import { useQuery } from 'react-query';
import { getCharacter } from '../services/books.services';

interface CharacterDetailInterface {
    url: string;
}

const CharacterDetail: React.FunctionComponent<CharacterDetailInterface> = ({ url }) => {
    const { data, isFetching } = useQuery(['character', url], () => getCharacter(url));

    return <List.Item>
        <Spin spinning={isFetching}>
            {data && (<>
                <List.Item.Meta
                    title={<Space>{data?.name}</Space>}
                />
                <Typography.Text>{data.aliases.map((alias : string) => <Tag>{alias}</Tag>)}</Typography.Text>
                <Descriptions layout={'vertical'}>
                    <Descriptions.Item label='gender'>{data?.gender}</Descriptions.Item>
                    <Descriptions.Item label='culture'>{data?.culture}</Descriptions.Item>
                    <Descriptions.Item label='born'>{data?.born}</Descriptions.Item>
                    <Descriptions.Item label='died'>{data?.died}</Descriptions.Item>
                </Descriptions>
            </>)}
        </Spin>
    </List.Item>
}

export default CharacterDetail;