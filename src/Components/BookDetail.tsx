import { Drawer, Spin, Descriptions, List, Tag, Typography, Badge, Space } from 'antd';
import { useQuery } from 'react-query';
import { getBook } from '../services/books.services';
import CharacterDetail from './CharacterDetail';

interface BookDetailInterface {
    url: string;
    onClose: () => void
}


const BookDetail: React.FunctionComponent<BookDetailInterface> = ({ url, onClose = () => { } }) => {
    const { data, isFetching, isError } = useQuery(['book', url], () => getBook(url));

    console.log(data, url);

    return <Drawer
        title='Détail'
        closable
        size='large'
        onClose={() => onClose()}
        visible
    >
        <Spin spinning={isFetching}>
            {data &&
                (<><Descriptions layout='vertical'>
                    <Descriptions.Item label={<Typography.Title level={5}>name</Typography.Title>}>{data?.name}</Descriptions.Item>
                    <Descriptions.Item label={<Typography.Title level={5}>isbn</Typography.Title>}>{data?.isbn}</Descriptions.Item>
                    <Descriptions.Item label={<Typography.Title level={5}>pages</Typography.Title>}>{data?.numberOfPages}</Descriptions.Item>
                    <Descriptions.Item label={<Typography.Title level={5}>publisher</Typography.Title>}>{data?.publisher}</Descriptions.Item>
                    <Descriptions.Item label={<Typography.Title level={5}>realeased date</Typography.Title>}>{data?.released}</Descriptions.Item>

                    <Descriptions.Item label={<Typography.Title level={5}>Authors</Typography.Title>}>{data?.authors.map((author: string) => <Tag>{author}</Tag>)}</Descriptions.Item>

                </Descriptions>
                    <Typography.Title level={5}><Space>Characters<Badge count={data.characters.length} overflowCount={10000}/></Space></Typography.Title>
                    <List
                        dataSource={data?.characters}
                        pagination={{ pageSize: 5, showSizeChanger: false, position: 'both' }}
                        renderItem={(item: string) => <CharacterDetail url={item} />}
                    />
                </>)
            }
        </Spin>

    </Drawer>
};

export default BookDetail;