import { useState } from 'react';
import { Table, Button, Space, Empty, Spin } from 'antd';
import { useQuery } from 'react-query';
import { getBooks } from '../services/books.services';
import BookDetail from './BookDetail';



const BookList: React.FunctionComponent = () => {
    const [ bookUrl, setBooksUrl] = useState('https://anapioficeandfire.com/api/books')
    const [ detailBookUrl, setDetailBookUrl ]= useState<string | null>(null);

    const { data, isLoading, isFetching } = useQuery(['books', bookUrl], async () => getBooks(bookUrl));

    const booksColumnDefinitions = [
        {
            title: 'Name',
            dataIndex: 'name'

        },
        {
            title: 'Pages',
            dataIndex: 'numberOfPages'
        },
        {
            title: 'Authors',
            dataIndex: 'authors'
        },
        {
            title: 'Released Date',
            dataIndex: 'released'
        },
        {
            title: 'Characters',
            dataIndex: 'characters',
            render: (items : string[]) => items.length
        },
        {
            title: null,
            dataIndex: 'url',
            render: (item: string) => (<Button onClick={() => setDetailBookUrl(item)}>Détail</Button>)
        }
    ];

    if(isLoading) return <Spin spinning><Empty /></Spin>
    
    const { data: books, links} = data as { data : any, links: any};

    return <>
        <Table
            loading={isFetching}
            columns={booksColumnDefinitions}
            dataSource={isLoading ? [] : books}
            pagination={false}
            footer={() => (
                <Space>
                    {links.has('rel','first') && <Button onClick={() => setBooksUrl(links.rel('first')[0].uri)}>First</Button> }
                    {links.has('rel','prev') && <Button onClick={() => setBooksUrl(links.rel('prev')[0].uri)}>Prev</Button> }
                    {links.has('rel','next') && <Button onClick={() => setBooksUrl(links.rel('next')[0].uri)}>Next</Button> }
                    {links.has('rel','last') && <Button onClick={() => setBooksUrl(links.rel('last')[0].uri)}>Last</Button> }
                </Space>
            )}
        />
        { detailBookUrl && <BookDetail url={detailBookUrl} onClose={() => setDetailBookUrl(null)} /> }
    </>

}

export default BookList;