import 'antd/dist/antd.css';
import { Layout, Typography } from 'antd';
import BookList from './Components/BookList';
import { ReactQueryDevtools } from 'react-query/devtools'

import { QueryClient, QueryClientProvider } from 'react-query';

const queryClient = new QueryClient({ defaultOptions: { queries: { staleTime: 60000 } } });

const App: React.FunctionComponent = () => (
  <QueryClientProvider client={queryClient}>
    <Layout>
      <Layout.Header>
        <Typography.Title style={{ color: 'white' }}>Ice and Fire</Typography.Title>
      </Layout.Header>
      <Layout.Content>
        <BookList />
      </Layout.Content>
      <Layout.Footer>
        <ReactQueryDevtools initialIsOpen={false} />
      </Layout.Footer>
    </Layout>
  </QueryClientProvider>
);

export default App
