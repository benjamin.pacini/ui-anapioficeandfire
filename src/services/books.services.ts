import axios from 'axios';
import LinkHeader from 'http-link-header';


export const getBooks = (url: string)  => axios.get(url).then(({ data, headers })=> {
    const links = LinkHeader.parse(headers.link);
    return {
        data,
        links
    };
});

export const getBook = (url : string) => axios.get(url).then(({ data }) => { console.log(data); return data});

export const getCharacter = (url : string) => axios.get(url).then(({ data }) => { console.log(data); return data});